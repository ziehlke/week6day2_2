package Zadanie1;

public enum PodatekProduktu {
    VAT8(8),
    VAT23(23),
    VAT5(5),
    NO_VAT(0);


    private double multiplier = 1;


    PodatekProduktu(double multiplier) {
        this.multiplier = multiplier;
    }

    public double getMultiplier() {
        return multiplier;
    }
}

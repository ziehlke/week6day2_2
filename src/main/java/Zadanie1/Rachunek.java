package Zadanie1;

import java.util.List;

public class Rachunek {
    private List<Produkt> listaZkupionych;


    public List<Produkt> getListaZkupionych() {
        return listaZkupionych;
    }


    public void wypiszRachunek() {

        for (Produkt p : listaZkupionych) {
            System.out.println(p);
        }
    }

    public double podsumujRachunekNetto() {
//        double sum = 0.0;
//        for (Produkt p : listaZkupionych
//        ) {
//            sum += p.cenaProduktu;
//        }
//        return sum;
        return listaZkupionych.stream().mapToDouble(Produkt::getCenaProduktu).sum();
    }





    public double podsumujRachunekBrutto() {
        double sum = 0.0;
        for (Produkt p : listaZkupionych) {
            sum += p.podajCeneBrutto();
        }
        return sum;
    }

    public double zwróćWartośćPodatku(){
        return podsumujRachunekBrutto() - podsumujRachunekNetto();
    }

//    stwórz metodę która zwróci informację o tym ile kosztowałyby wszystkie
// produkty gdyby posiadały podatek 8%,
// oraz gdyby posiadały podatek 23%

    public double rachunekGdyWszystki8(){
        double sum = 0.0;
        for (Produkt p: listaZkupionych) {
            sum += p.getCenaProduktu() + ( p.getCenaProduktu() * PodatekProduktu.VAT8.getMultiplier() / 100.0 );
        }
        return sum;
    }

    public double rachunekGdyWszystki23(){
        double sum = 0.0;
        for (Produkt p: listaZkupionych) {
            sum += p.getCenaProduktu() + ( p.getCenaProduktu() * PodatekProduktu.VAT23.getMultiplier() / 100.0 );
        }
        return sum;
    }



}

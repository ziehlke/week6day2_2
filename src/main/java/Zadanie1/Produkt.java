package Zadanie1;

//- nazwa produktu
//        - cena produktu (netto)
//        - ilość podatku (PodatekProduktu)
//
//        Dodaj w klasie produkt metody:
//        - gettery oraz settery do wszystkich pól
//        - metodę podajCeneBrutto():double -
//        która oblicza cenę brutto na podstawie ceny netto
//        i nałożonego podatku


public class Produkt {
    private double getCenaProduktu;
    private PodatekProduktu iloscPodatku;


    public double getCenaProduktu() {
        return getCenaProduktu;
    }

    public void setCenaProduktu(double cenaProduktu) {
        this.getCenaProduktu = cenaProduktu;
    }

    public PodatekProduktu getIloscPodatku() {
        return iloscPodatku;
    }

    public void setIloscPodatku(PodatekProduktu iloscPodatku) {
        this.iloscPodatku = iloscPodatku;
    }


    public double podajCeneBrutto() {
        return getCenaProduktu + (getCenaProduktu * (iloscPodatku.getMultiplier() / 100.0));

    }

}

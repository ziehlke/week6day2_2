import Zadanie3.Student;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class Main {

    public static void main(String[] args) {

        Map<Long, Student> mapaStudentow = new TreeMap<>();

        mapaStudentow.put(111L, new Student(111L, "Marian", "Kowal"));
        mapaStudentow.put(112L, new Student(112L, "Marian2", "Kowal2"));
        mapaStudentow.put(113L, new Student(113L, "Marian3", "Kowal3"));
        mapaStudentow.put(114L, new Student(114L, "Marian4", "Kowal4"));

        System.out.println(mapaStudentow.containsKey(100200L));
        System.out.println(mapaStudentow.get(100400L));
        System.out.println(mapaStudentow.size());

        for (Student s : mapaStudentow.values()
        ) {
            System.out.println(s);
        }


        for (Map.Entry<Long, Student> wpis : mapaStudentow.entrySet()) {

            System.out.println(wpis.getKey() + " : " + wpis.getValue());

        }


    }
}

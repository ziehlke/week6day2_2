package Zadanie2;

import Zadanie1.Produkt;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

public class Poczekalnia {
    private PriorityQueue<Klient> kolejka;

    Queue<Klient> kolejkaPriorytetowa = new LinkedList<>();







    public Poczekalnia() {
        this.kolejka = new PriorityQueue<>(new Comparator<Klient>() {
            @Override
            public int compare(Klient o1, Klient o2) {

                if (o1.isPriorytet() && !o2.isPriorytet()) {
                    return -1;
                } else if (o2.isPriorytet() && !o1.isPriorytet()) {
                    return 1;
                } else if (o1.getCzasPrzybycia().isAfter(o2.getCzasPrzybycia())) {
                    return 1;
                } else if (o1.getCzasPrzybycia().isBefore(o2.getCzasPrzybycia())) {
                    return -1;
                }
                return 0;

            }
        });
    }


    public void dodajKlienta(String imie, boolean czyPriorytet) {
        kolejka.add(new Klient(imie, czyPriorytet));
    }

//- pobierzKlienta():Klient - która zwraca następnego w kolejce klienta

    public Klient pobierzKlienta(){
        return kolejka.poll();
    }


//- wypiszKolejnoKlientów():void - metoda która wyciąga z kolejki klientów dopóki
// w kolejce są jacyś klienci. Wraz 'wyciąganiem' klientów wypisuj ich informacje na ekran.

    public void wypiszKolejnoKlientów(){

        while (!kolejka.isEmpty()) {
            System.out.println(kolejka.poll());
        }
    }



    public static void main(String[] args) {

        //- dodajKlienta(String imie, boolean czyPriorytet):void - która dodaje klienta o podanym imieniu z
        // informacją czy jest priorytetyzowany. W metodzie stwórz instancje klasy Klient i ustaw mu wartości
        // wszystkich pól. Czas dodania ustaw na 'now()' czyli moment dodania go do kolejki.
        Poczekalnia poczekalnia = new Poczekalnia();
        poczekalnia.dodajKlienta("Mariolka1", true);
        poczekalnia.dodajKlienta("Mariolka2", false);
        poczekalnia.dodajKlienta("Mariolka3", false);
        poczekalnia.dodajKlienta("Mariolka4", true);
        poczekalnia.dodajKlienta("Mariolka5", true);
        poczekalnia.dodajKlienta("Mariolka6", true);


    }

}

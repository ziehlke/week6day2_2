package Zadanie2;

/*Stwórz klasę Klient, która posiada:
        - imie klienta (wyłącznie dla celów prezentacji - żeby później łatwiej nam było czytać poprawność wyników)
        - czas przybycia
        - czy jest priorytetem (tak lub nie)*/


import java.time.LocalDateTime;

public class Klient {

    private String imie;
    private LocalDateTime czasPrzybycia;

    public Klient() {
    }

    public Klient(String imie, boolean priorytet) {
        this.imie = imie;
        this.czasPrzybycia = LocalDateTime.now();
        this.priorytet = priorytet;
    }


    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public LocalDateTime getCzasPrzybycia() {
        return czasPrzybycia;
    }

    public void setCzasPrzybycia(LocalDateTime czasPrzybycia) {
        this.czasPrzybycia = czasPrzybycia;
    }

    public boolean isPriorytet() {
        return priorytet;
    }

    public void setPriorytet(boolean priorytet) {
        this.priorytet = priorytet;
    }

    private boolean priorytet;

}

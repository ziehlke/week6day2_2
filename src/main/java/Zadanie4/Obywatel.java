package Zadanie4;

/*Stwórz klasę Obywatel, która posiada:
        pesel, imie, nazwisko,
        Stwórz klasę RejestrObywateli .

        */


public class Obywatel {

    private String pesel, imie, nazwisko;

    public Obywatel(String pesel, String imie, String nazwisko) {
        this.pesel = pesel;
        this.imie = imie;
        this.nazwisko = nazwisko;
    }

    public Obywatel() {
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }


    public int podajRokUrzodzenia(){
        return Integer.parseInt(getPesel().substring(0, 2));
    }
}

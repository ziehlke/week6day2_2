package Zadanie4;


/*
która posiada wewnątrz (jako pole) mapę obywateli
Dodaj do Rejestru Obywateli metody:
        - dodajObywatela(String pesel, String imie, String nazwisko):void
        - znajdźObywateliUrodzonychPrzed(int rok):List<Obywatel> */


import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class RejestrObywateli {

    Map<String, Obywatel> mapaObywateli;


    public void dodajObywatela(String pesel, String imie, String nazwisko) {
        mapaObywateli.put(pesel, new Obywatel(pesel, imie, nazwisko));
    }


    public List<Obywatel> znajdźObywateliUrodzonychPrzed(int rok) { //zakldamy, ze tylko 2 cyfry przed 2000 :)
        List<Obywatel> zbiorUrodzonychPrzed = new LinkedList<>();

        for (Obywatel o : mapaObywateli.values()) {

            if (Integer.parseInt(o.getPesel().substring(0, 2)) < rok) {
                zbiorUrodzonychPrzed.add(o);
            }
        }
        return zbiorUrodzonychPrzed;
    }


    public List<Obywatel> znajdźObywateliUrodzonychPrzedWJava8(int rok) { //zakldamy, ze tylko 2 cyfry przed 2000 :)
        return mapaObywateli.values().stream()
                .filter(o -> o.podajRokUrzodzenia() < rok)
                .collect(Collectors.toList());
    }

//        - znajdźObywateliZRokuZImieniem(int rok, String imie):List<Obywatel>

    public List<Obywatel> znajdźObywateliZRokuZImieniem(int rok, String imie) {

        return mapaObywateli.values().stream()
                .filter(o -> o.getImie() == imie && o.podajRokUrzodzenia() == rok)
                .collect(Collectors.toList());

    }


    //        - znajdźObywatelaPoNazwisku(String nazwisko):List<Obywatel>
    public List<Obywatel> znajdźObywatelaPoNazwisku(String nazwisko) {

        return mapaObywateli.values().stream()
                .filter(o -> o.getNazwisko().equals(nazwisko))
                .collect(Collectors.toList());
    }


    //        - znajdźObywatelaPoPeselu(String pesel):Obywatel
    public Optional<Obywatel> znajdźObywatelaPoPeselu(String pesel) {
        return mapaObywateli.values().stream()
                .filter(o -> o.getPesel().equals(pesel))
                .findFirst();
    }


}

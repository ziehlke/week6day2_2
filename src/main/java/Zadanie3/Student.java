package Zadanie3;

public class Student {
    private long numerIndexu;
    String imie, nazwisko;

    @Override
    public String toString() {
        return "Student{" +
                "numerIndexu=" + numerIndexu +
                ", imie='" + imie + '\'' +
                ", nazwisko='" + nazwisko + '\'' +
                '}';
    }

    public Student() {
    }

    public long getNumerIndexu() {
        return numerIndexu;
    }

    public void setNumerIndexu(long numerIndexu) {
        this.numerIndexu = numerIndexu;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public Student(long numerIndexu, String imie, String nazwisko) {
        this.numerIndexu = numerIndexu;
        this.imie = imie;
        this.nazwisko = nazwisko;
    }
}

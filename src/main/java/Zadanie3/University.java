package Zadanie3;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class University {

    private Map<Long, Student> studentMap = new HashMap<>();


    public void addStudent(long indexNumber, String name, String surname) {


        if (studentMap.containsKey(indexNumber)) {
            System.out.println("taki index jest juz uzyty");
            while (studentMap.containsKey(indexNumber)) indexNumber++;
        }
        Student nowy = new Student(indexNumber, name, surname);
        studentMap.put(indexNumber, nowy);
    }


    public boolean containsStudent(long indexNumber) {
        return studentMap.containsKey(indexNumber);
    }


    public Optional<Student> getStudent(long index) throws NoSuchStudentException {
        if (!studentMap.containsKey(index)) throw new NoSuchStudentException();
        return Optional.ofNullable(studentMap.get(index));
    }

    public int sudentsNumber() {
        return studentMap.size();
    }

    public int uniqueStudents() {
        return (int) studentMap.values().stream().distinct().count();
    }

    public void listAllStudents() {
        studentMap.values().stream().distinct().forEach(System.out::println);
    }


}
